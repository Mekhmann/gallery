import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    data: []
  },
  getters: {
    gdata : state => {
      return state.data 
    }
  },
  mutations: {
    setPictures : (state, payload) => {
      state.data = payload
    }
  },
  actions: {
    getPictures : async (context) => {
      Vue.prototype.$Progress.start()
      await fetch('https://jsonplaceholder.typicode.com/photos?_limit=24')
                    .then (function (response) {
                      return response.json()
                    })
                    .then (function(json){
                      context.commit('setPictures', json);
                    })
                    .catch(function (error) {
                      console.log('error', error)
                    })
      setTimeout(function (){
        Vue.prototype.$Progress.finish()
      }, 500);
    }
  },
  modules: {
  }
})
